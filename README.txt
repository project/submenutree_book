Extend submenutree to display child book pages as teasers or links.
 
When using submenutree on a node that is part of a book,
child pages will be rendered using submenutree settings,
and the normal book navigation will be repressed.
 
